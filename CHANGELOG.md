# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2022-03-18
### Fixed
- Support queues without queue worker

### Changed
- Change license to GPLv2 to comply with Drupal.org requirements

## [1.0.0] - 2021-05-18
Initial release

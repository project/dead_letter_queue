<?php

namespace Drupal\dead_letter_queue_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\dead_letter_queue\Queue\DeadLetterQueueInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * A confirmation form for resetting the amount of tries of a queue item.
 */
class ConfirmItemResetTriesForm extends ConfirmFormBase {

  /**
   * The queue name.
   *
   * @var string
   */
  protected $queueName;

  /**
   * The queue item ID.
   *
   * @var int
   */
  protected $queueItemId;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->queueFactory = $container->get('queue');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to reset the amount of tries of queue item %queue_item?', ['%queue_item' => $this->queueItemId]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('queue_ui.inspect', ['queueName' => $this->queueName]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'queue_ui_confirm_item_reset_tries_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?string $queueName = NULL, ?int $queueItemId = NULL) {
    assert($queueName !== NULL, 'A queue name must be provided.');
    assert($queueItemId !== NULL, 'A queue item ID must be provided.');

    $this->queueName = $queueName;
    $this->queueItemId = $queueItemId;

    $queueUi = $this->queueFactory->get($this->queueName);

    if (!$queueUi instanceof DeadLetterQueueInterface) {
      throw new NotFoundHttpException();
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState): void {
    /** @var \Drupal\dead_letter_queue\Queue\DeadLetterQueueInterface $queue */
    $queue = $this->queueFactory->get($this->queueName);
    $queue->resetItemTries($this->queueItemId);

    $this->messenger->addMessage(sprintf('Reset tries of queue item %s', $this->queueItemId));
    $formState->setRedirectUrl(Url::fromRoute('queue_ui.inspect', ['queueName' => $this->queueName]));
  }

}

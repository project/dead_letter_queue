<?php

namespace Drupal\dead_letter_queue_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\dead_letter_queue_ui\DeadLetterQueueUiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * The form for viewing dead letters.
 */
class DeadLettersForm extends FormBase {

  /**
   * The queue UI manager.
   *
   * @var \Drupal\queue_ui\QueueUIManager
   */
  protected $queueUIManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->queueUIManager = $container->get('plugin.manager.queue_ui');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dead_letter_queue_ui_dead_letters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?string $queueName = NULL) {
    assert($queueName !== NULL, 'A queue name must be provided.');

    $queueUi = $this->queueUIManager->fromQueueName($queueName);

    if (!$queueUi instanceof DeadLetterQueueUiInterface) {
      throw new NotFoundHttpException();
    }

    $rows = [];
    foreach ($queueUi->getDeadLetters($queueName) as $item) {
      $operations = [];

      foreach ($queueUi->getOperations() as $op => $title) {
        $operations[] = [
          'title' => $title,
          'url' => Url::fromRoute('queue_ui.inspect.' . $op, ['queueName' => $queueName, 'queueItem' => $item->item_id]),
        ];
      }

      $rows[] = [
        'id' => $item->item_id,
        'expires' => ($item->expire ? date(DATE_RSS, $item->expire) : $item->expire),
        'created' => date(DATE_RSS, $item->created),
        'tries' => $item->tries,
        'operations' => [
          'data' => [
            '#type' => 'dropbutton',
            '#links' => $operations,
          ],
        ],
      ];
    }

    return [
      'table' => [
        '#type' => 'table',
        '#header' => [
          'id' => $this->t('Item ID'),
          'expires' => $this->t('Expires'),
          'created' => $this->t('Created'),
          'tries' => $this->t('Tries'),
          'operations' => $this->t('Operations'),
        ],
        '#rows' => $rows,
        '#empty' => $this->t("This queue doesn't have any dead letters."),
      ],
      'pager' => [
        '#type' => 'pager',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
  }

}

<?php

namespace Drupal\dead_letter_queue_ui;

use Drupal\queue_ui\QueueUIInterface;

/**
 * An interface for dead letter queue UI plugins.
 */
interface DeadLetterQueueUiInterface extends QueueUIInterface {

  /**
   * Retrieve the dead letters for a specified queue.
   *
   * @param string $queueName
   *   The name of the queue being inspected.
   *
   * @return array
   *   An array of dead letter queue items.
   */
  public function getDeadLetters(string $queueName): array;

  /**
   * Retrieve the number of dead letters for a specified queue.
   *
   * @param string $queueName
   *   The name of the queue being inspected.
   *
   * @return int
   *   The number of dead letter queue items.
   */
  public function getNumberOfDeadLetters(string $queueName): int;

}

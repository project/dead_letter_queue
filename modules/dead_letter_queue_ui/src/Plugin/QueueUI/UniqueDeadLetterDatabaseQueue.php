<?php

namespace Drupal\dead_letter_queue_ui\Plugin\QueueUI;

/**
 * A queue UI plugin for unique dead letter database queues.
 *
 * @QueueUI(
 *   id = "unique_dead_letter_database_queue",
 *   class_name = "UniqueDeadLetterDatabaseQueue"
 * )
 */
class UniqueDeadLetterDatabaseQueue extends DeadLetterDatabaseQueueUi {

  public const TABLE_NAME = \Drupal\dead_letter_queue_unique\Queue\UniqueDeadLetterDatabaseQueue::TABLE_NAME;

}

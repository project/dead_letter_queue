<?php

namespace Drupal\dead_letter_queue_unique\Queue;

use Drupal\dead_letter_queue\Queue\DeadLetterQueueDatabaseFactory;

/**
 * Factory class for generating unique dead letter database queues.
 */
class UniqueDeadLetterQueueDatabaseFactory extends DeadLetterQueueDatabaseFactory {

  /**
   * {@inheritdoc}
   */
  public function get($name) {
    return new UniqueDeadLetterDatabaseQueue(
      $name,
      $this->connection,
      $this->queueManager,
      $this->configFactory,
      $this->logger
    );
  }

}

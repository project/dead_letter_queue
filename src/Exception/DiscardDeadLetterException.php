<?php

namespace Drupal\dead_letter_queue\Exception;

/**
 * Exception to discard the item instead of storing it as a dead letter.
 */
class DiscardDeadLetterException extends \RuntimeException {
}

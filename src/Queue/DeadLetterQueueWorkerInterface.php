<?php

namespace Drupal\dead_letter_queue\Queue;

use Drupal\Core\Queue\QueueWorkerInterface;

/**
 * Interface for a dead letter queue worker.
 */
interface DeadLetterQueueWorkerInterface extends QueueWorkerInterface {

  /**
   * Act on a dead letter item.
   *
   * @param mixed $data
   *   The queue item data.
   */
  public function handleDeadLetter($data);

}

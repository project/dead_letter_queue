<?php

namespace Drupal\dead_letter_queue\Queue;

use Drupal\Core\Queue\QueueInterface;

/**
 * Interface for a dead letter queue.
 */
interface DeadLetterQueueInterface extends QueueInterface {

  /**
   * Reset the number of tries for a given item.
   */
  public function resetItemTries(int $itemId): void;

  /**
   * Get the number of tries for a given item.
   */
  public function getMaxTries(): int;

}

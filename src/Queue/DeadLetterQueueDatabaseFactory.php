<?php

namespace Drupal\dead_letter_queue\Queue;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueDatabaseFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;

/**
 * Factory class for generating dead letter database queues.
 */
class DeadLetterQueueDatabaseFactory extends QueueDatabaseFactory {

  /**
   * The queue manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a DeadletterQueueDatabaseFactory object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queueManager
   *   The queue manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger.
   */
  public function __construct(
    Connection $connection,
    QueueWorkerManagerInterface $queueManager,
    ConfigFactoryInterface $configFactory,
    LoggerChannelInterface $logger
  ) {
    parent::__construct($connection);
    $this->queueManager = $queueManager;
    $this->configFactory = $configFactory;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function get($name) {
    return new DeadLetterDatabaseQueue(
      $name,
      $this->connection,
      $this->queueManager,
      $this->configFactory,
      $this->logger
    );
  }

}
